const getData = async (url) => {
    const res = await fetch(url);

    if(!res.ok){
       throw new Error(`Error status: ${res.status} from ${res.url}`);
    }

    return await res.json();
};

function showTask() {
    getData('https://reqres.in/api/users?per_page=12').then(data => {
        data.data.forEach(item => {
            showUsers(item);
        });
    });
}


const showUsers = (user) => {
    let cardsWrapper = document.querySelector('.cards');
    let card = document.createElement('li');
    card.innerHTML = `
        <div class="card" style="width: 18rem;">
            <img src="${user.avatar}" class="card-img-top" alt="${user.first_name}">
            <div class="card-body">
                <h5 class="card-title">ФИО</h5>
                <p class="card-text">Имя: <b>${user.first_name}</b></p>
                <p class="card-text">Фамилия: <b>${user.last_name}</b></p>
            </div>
        </div>
    `;
    cardsWrapper.append(card);
}

showTask();