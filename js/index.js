let formRegistration = document.querySelector('.form__registratuion');
let formEntrance  = document.querySelector('.form__entrance '); 

const gettLocalStorage = () => JSON.parse(localStorage.getItem('posts'));

const setLocalStorage = (post) => {
    if (gettLocalStorage() === null) {
        let posts = [];
        posts.push(post);
        localStorage.setItem('posts', JSON.stringify(posts));
      } else {
        let posts = gettLocalStorage();
        posts.push(post);
        localStorage.setItem('posts', JSON.stringify(posts));
      }
}

function validation(form) {

    function removeError(input) {
        const parent = input.parentNode;
        if (parent.classList.contains('error')) {
            parent.querySelector('.error-label').remove();
            parent.classList.remove('error');
        } else {
            return;
        }
    }

    function createError(input, text) {
    
        const parent = input.parentNode;
        const errorLabel = document.createElement('label');
        errorLabel.classList.add('error-label');
        errorLabel.textContent = text;

        parent.classList.add('error');

        parent.append(errorLabel);
    }


    let result = true;

    const allInputs = form.querySelectorAll('input');
    
    for (const input of allInputs) {
        removeError(input);
        if (!input.value) {
            createError(input, 'Поле обязательно для заполнения');
            result = false;
        }
        let checkboxPerent = input.parentNode.parentNode;
        if ( input.type == 'checkbox' && !input.checked) {
            createError(checkboxPerent, 'Поле обязательно для заполнения');
            result = false;
        } else if (input.type == 'checkbox' && input.checked) {
            removeError(checkboxPerent);
        }
    }

    return result;
}


function validateEmail(email) { 
    const re=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
    return re.test(String(email).toLowerCase());
}


document.querySelector('.form__registratuion-link').addEventListener('click', () => {
    document.querySelector('.form__registratuion').style.display = 'none';
    document.querySelector('.form__entrance').style.display = 'block';
});

document.querySelector('.form__entrance-link').addEventListener('click', () => {
    document.querySelector('.form__registratuion').style.display = 'block';
    document.querySelector('.form__entrance').style.display = 'none';
});

const newUser = (e)=> {
    let user = {
        email: document.querySelector('.form__input_email').value,
        password: document.querySelector('.form__input_password').value,
        agree: document.querySelector('#modern-checkbox').checked,
    }

    if (validateEmail(user.email)) {
        setLocalStorage(user);
        alert("Успешно прошли регистрацию!");
        e.target.reset(); 
        formRegistration.style.display = 'none';
        formEntrance.style.display = 'block';
    } else {
        e.target.reset(); 
        alert("не корректно введен email!");
    }
}
  
formRegistration.addEventListener('submit', (e)=> {
    e.preventDefault();
    if (validation(formRegistration)) {
        newUser(e);
    }
});

formEntrance.addEventListener('submit', (e)=> {
    e.preventDefault();

    if (validation(formEntrance)) {
        let user = {
            email: document.querySelector('.form__input_email-entrance').value,
            password: document.querySelector('.form__input_password-entrance').value,
            agree: document.querySelector('#modern-checkbox1').checked,
        }
        const userAuth =  gettLocalStorage().filter (item => item.email == user.email && item.password == user.password);
        
        if (userAuth.length) {
            document.location.href="./pages/home.html";
            alert('нажми "Ок", чтобы войти на сайт');
            e.target.reset(); 
        } else {
            alert('Логин или пароль указан не верно!');
            e.target.reset();
        }

    }
});


